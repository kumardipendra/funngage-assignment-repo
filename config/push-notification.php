<?php

return array(

    'appNameIOS'     => array(
        'environment' =>'production',
        'certificate' =>public_path().'/ThoughtTrackerDistPush.pem',
        'passPhrase'  =>'',
        'service'     =>'apns'
    ),
    'appNameAndroid' => array(
        'environment' =>'production',
        'apiKey'      =>'AIzaSyB7meGYSwy-yXE2zQsuytMS1wbPZkWojp0',
        'service'     =>'gcm'
    )

);