<?php

namespace App\Library;

class GetApiError {

    public function __construct() {
        $this->error = array(
            '1001' => "Exception error!",
            '1003' => "Invalid User!",
            '1004' => "You have successfully logged in.",
            '1005' => 'You entered the wrong credentials.',
            '1006' => "You entered the wrong password.",
            '1007' => "It seems that you have not uploaded any profile picture.",
            '1008' => 'User not found!',
            '1009' => 'User account is not activated.',
            '1010' => 'User has not selected any category yet.',
            '1011' => 'Invalid Credentials. Please check.',
            '1012' => 'The old password you have entered is incorrect.',
            '1013' => 'Facebook user can not retrive the password.',
            '1015' => 'No more custom slot available.',
            '1016' => 'The new password matches with the old password, please choose a different password.',
            '1017' => 'Maximum limit reached!',
            '1018' => '"You have successfully logged out.',
            '10' => 'Thought with same name has already in use.'
        );
    }

    public function getapplicationerror($error_code) {
        if (array_key_exists($error_code, $this->error)) {
            return $this->error[$error_code];
        }
    }

}
