<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserToken extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_token';
    protected $primaryKey = 'id';

    public function register_device_token($user_id, $request, $user_access_token) {
        static::where('device_token', $request->device_token)->delete();
        $device_token = new UserToken();
        $device_token->user_id = $user_id;
        $device_token->device_token = $request->device_token;
        $device_token->user_access_token = $user_access_token;
        $device_token->save();
        return $device_token;
    }

    public function Logout($request) {

        static::where('user_id', $request->input('userId'))
                ->where('user_access_token', $request->input('userAccessToken'))
                ->delete();
        return 1018;
    }

}
