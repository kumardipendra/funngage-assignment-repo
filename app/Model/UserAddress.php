<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\User;

class UserAddress extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_address';
    protected $primaryKey = 'id';

    public function getAddressInsert($address) {



        for ($j = 0; $j < sizeof($address); $j++) {
            $geteducount = self::where('address1', '=', $address[$j]['address1'])->where('pincode', '=', $address[$j]['pincode'])->where
                            ('city', '=', $address[$j]['city'])->where('state', '=', $address[$j]['state'])->count();
            if ($geteducount == 0) {
                $data[] = array('address1' => $address[$j]['address1'], 'pincode' => $address[$j]['pincode'], 'city' => $address[$j]['city'], 'state' => $address[$j]['state']
                );
            }
        }
        if(!empty($data))
        {
        static::insert($data);
        $data = array();
        }
        return true;
    }
}
