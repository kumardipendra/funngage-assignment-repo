<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Education extends Model {

   
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'education';
    protected $primaryKey = 'id';
    protected $fillable = ['year', 'degree', 'university', 'college'];
    

   public $timestamps = false;
public function getuserEducation($education) {
        for($j=0;$j<sizeof($education);$j++)
        {
           $geteducount = self::where('year','=', $education[$j]['year'])->where('degree','=',$education[$j]['degree'])->where
                   ('university','=', $education[$j]['university'])->where('college','=',$education[$j]['college'])->count();
           if($geteducount==0)
           {
               $data[] = array('year' => $education[$j]['year'], 'degree' => $education[$j]['degree'], 'university' => $education[$j]['university'], 'college' => $education[$j]['college']
                );

           }    
        }
        if(!empty($data))
        {
        static::insert($data);
        $data = array();
        }
        return true; 
    }
    
}