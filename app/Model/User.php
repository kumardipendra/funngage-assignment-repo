<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Hash;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

    use Authenticatable,
        Authorizable,
        CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['email', 'password', 'user_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function register($request) {

        $user = new User;
        $user->email = strip_tags($request->input('email', ''));
        $user->password = bcrypt(strip_tags($request->input('password')));
        $user->save();

        $user = $user->where("user_id", $user->user_id)
                ->select('user_id', 'email')
                ->first();

        if (is_object($user)) {
            $user->user_access_token = $this->add_device_token($user->user_id, $request);
            unset($user->created_at);
            unset($user->updated_at);
        }

        return $user;
    }

    private function add_device_token($user_id, $request) {
        $get_device_token = new UserToken();
        $user_access_token = md5(time() . uniqid());
        $device_token = $get_device_token->register_device_token($user_id, $request, $user_access_token);
        if (is_object($device_token)) {
            return $user_access_token;
        }
    }

    private function get_device_token($user_id, $user_access_token) {
        $get_device_token = new UserToken();
        $device_token = $get_device_token->device_token($user_id, $user_access_token);
        return $device_token;
    }

    public function postLogin(Request $request) {

        try {
            $validation_rules = array(
                'login_type' => 'required',
                'email' => 'email|required'
            );

            if ($request->login_type == 'facebook') {
                $validation_rules['facebook_id'] = 'required';
            } else {
                $validation_rules['password'] = 'required';
            }
            $validator = Validator::make($request->all(), $validation_rules);
            if ($validator->fails()) {
                return $this->responce->ResponceValidationError($validator);
            } else {

                $user = $this->apiModel->getLogin($request);
                if ($user == 1011) {
                    return $this->responce->ResponceError('1011');
                } else {
                    return $this->responce->ResponceSuccess($user);
                }
            }
        } catch (Exception $e) {
            return $this->responce->ResponceExceptionError($e->message());
        }
    }

    public function getLogin($request) {

        $email = $request->input('email');
        $password = $request->input('password', '');

        $user = static::select('user_id', 'email')->where('status', 0);

       
            $user = $user->where('email', $email)
                    ->where('is_deleted', 0)
                    ->where('status', 0)
                    ->select('user_id', 'password', 'email')
                    ->first();

            if (is_object($user)) {
                if (!Hash::check($password, $user->password)) {
                    return 1011;
                }
            } else {
                return 1011;
            }
        
        if (is_object($user)) {
            $user->user_access_token = $this->add_device_token($user->user_id, $request);
            unset($user->created_at);
            unset($user->updated_at);
            return $user->toArray();
        }
    }

}