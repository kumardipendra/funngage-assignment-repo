<?php

namespace App\Http\Controllers\api;

class Responce {

    protected $error;

    public function __construct() {
        $this->error = new \App\Library\GetApiError();
    }

    //////////////////////////////////////// Common Method ////////////////////////////////////////////

    public function ResponceSuccess($data = array(), $message = '') {
        $data = array(
            'status' => "1",
            'response_code' => "200",
            'data' => (object) $data,
            'message' => $message,
        );
        return $this->GetJsonData($data);
    }

    public function ResponceSuccessStatus($data = array(), $message = '') {
        $data = array(
            'status' => "1",
            'response_code' => "200",
            'data' => $data,
            'message' => $message,
        );
        return $this->GetJsonData($data);
    }

    public function ResponceSuccessNotification($data = array(), $message = '', $notification = '') {
        $data = array(
            'status' => "1",
            'response_code' => "200",
            'message' => $message,
            'notificationStatus' => $notification
        );
        return $this->GetJsonData($data);
    }

    public function ResponceError($response_code,$error_code=0) {
        $data = array(
            'status' => "0",
            'response_code' => $response_code,
            'data' => (object) array(),
            'errorCode' =>$error_code,
            'message' => $this->error->getapplicationerror($response_code),
        );
        return $this->GetJsonData($data);
    }

    public function ResponceValidationError($validator) {
        $data = array(
            'status' => "0",
            'response_code' => '1001',
            'data' => (object) array(),
            'message' => $validator->messages()->first(),
        );
        return $this->GetJsonData($data);
    }

    public function ResponceExceptionError($message) {
        $data = array(
            'status' => "0",
            'response_code' => 1000,
            'data' => (object) array(),
            'message' => $message,
        );
        return $this->GetJsonData($data);
    }

    public function GetJsonData($data) {
        return json_encode($data);
    }

}
