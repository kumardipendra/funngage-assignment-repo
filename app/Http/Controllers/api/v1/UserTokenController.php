<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Controllers\api\Responce;
use App\Library\GetApiError;
use Request;
use trans;
use App\Model\UserToken;
use Illuminate\Support\Facades\Validator;
class UserTokenController extends Controller {

    protected $responce;

    public function __construct() {

        $this->middleware('App\Http\Middleware\ApiAuth', ['except' => ['pushNotification']]);

        $this->responce = new Responce();
        $request = Request::instance();
        $this->postData = json_decode($request->getContent(), true);
        $this->error = new GetApiError();
    }

}
