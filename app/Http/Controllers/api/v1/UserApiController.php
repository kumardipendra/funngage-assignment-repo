<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Library\GetApiError;
use App\Model\User;
use App\Model\UserToken;
use App\Http\Controllers\api\Responce;
use trans;


class UserApiController extends Controller {

    protected $responce;

    public function __construct() {

        $this->middleware('App\Http\Middleware\ApiAuth', ['except' => ['postRegisteration', 'postLogin', 'postLogout']]);

        $this->responce = new Responce();
        $this->apiModel = new User();
        $this->error = new GetApiError();
    }

    public function postRegisteration(Request $request) {

        try {
            $validation_rules = array(
                'email' => 'email|required|unique:users',
                'password' => 'required|min:8,max:20'
            );

            $validator = Validator::make(
                            $request->all()
                            , $validation_rules
            );

            if ($validator->fails()) {
                return $this->responce->ResponceValidationError($validator);
            } else {
                $user = $this->apiModel->register($request);
                return $this->responce->ResponceSuccess($user, trans('messages.signupsucessfully'));
            }
        } catch (Exception $e) {
            return $this->responce->ResponceExceptionError($e->message);
        }
    }

    public function postLogin(Request $request) {
        try {
            $validation_rules = array(
                'email' => 'email|required',
            'password' => 'required'

            );
            $validator = Validator::make($request->all(), $validation_rules);
            if ($validator->fails()) {
                return $this->responce->ResponceValidationError($validator);
            } else {

                $user = $this->apiModel->getLogin($request);
                if ($user == "1011") {
                    return $this->responce->ResponceError('1011');
                } else {
                    return $this->responce->ResponceSuccess($user);
                }
            }
        } catch (Exception $e) {
            return $this->responce->ResponceExceptionError($e->message());
        }
    }



    public function postLogout(Request $request) {
        try {
            $validation_rules = array(
                'userId' => 'required|exists:users,user_id',
                'userAccessToken' => 'required'
            );
            $validator = Validator::make($request->all(), $validation_rules);
            if ($validator->fails()) {
                return $this->responce->ResponceValidationError($validator);
            } else {
                $UserTokenObj = new UserToken();
                $user = $UserTokenObj->Logout($request);
                    return $this->responce->ResponceSuccess($user, trans('messages.sucessfullylogout'));
            }
        } catch (Exception $e) {
            return $this->responce->ResponceExceptionError($e->message());
        }
    }
}