<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Controllers\api\Responce;
use trans;
use App\Library\GetApiError;
use App\Model\Education;
use App\Model\UserAddress;
use Request;
use Illuminate\Support\Facades\Validator;

class UserSelectedCategoryController extends Controller {

    protected $responce;

    public function __construct() {

        $this->middleware('App\Http\Middleware\ApiAuth', ['except' => ['postInsertEducationAddress']]);

        $this->responce = new Responce();
        $request = Request::instance();
        $this->postData = json_decode($request->getContent(), true);
        $this->error = new GetApiError();
    }

    public function postInsertEducationAddress() {
        try {
            $validation_rules = array(
                'address' => 'required',
                'education' => 'required',
            );
            $messages = array(
                'address.required' => trans('messages.notemptyaddress'),
                'education.required' => trans('messages.notemptyeducation'),
            );
            $validator = Validator::make($this->postData, $validation_rules, $messages);
            if ($validator->fails()) {
                return $validator->errors()->all();
            } else {
                $education = $this->postData['education'];
                $address = $this->postData['address'];
                $useraducationObj = new Education;
                $useraddress = new UserAddress; 
                
                    $insertedu = $useraducationObj->getuserEducation($education);
                    $insertaddress = $useraddress->getAddressInsert($address);
                    if ($insertedu || $insertaddress) {
                        return $this->responce->ResponceSuccess(null, trans('messages.insertedsucessfully'));
                    }
               
            }
        } catch (Exception $e) {
            return $this->responce->ResponceExceptionError($e->message());
        }
    }


}
