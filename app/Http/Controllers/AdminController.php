<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Redirect;
use App\Library\GetApiError;
use App\Http\Controllers\api\Responce;
use App\Http\Controllers\Controller;
use Auth;
use App\Model\User;
use App\Model\Categories;
use App\Model\UserTap;
use Yajra\Datatables\Datatables;
use Hash;
use DB;

class AdminController extends Controller {

    public function __construct() {
        $this->middleware('auth', ['except' => ['index', 'postLogin', 'getLogout']]);
        $this->error = new GetApiError();
        $this->responce = new Responce();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('auth/login');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dashboard() {
        return view('dashboard');
    }

    public function postLogin(Request $request) {


        try {
            $validation_rules = array(
                'email' => 'email|required|exists:users',
                'password' => 'required|min:6'
            );

            $validator = Validator::make(
                            $request->all()
                            , $validation_rules
            );

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            } else {
                $email = $request->email;
                $password = $request->password;
                if (Auth::attempt(['email' => $email, 'password' => $password, 'role' => 1])) {
                    Auth::user();
                    return redirect('admin/normal-users-list');
                } else {
                    return Redirect::back()->withErrors(trans('messages.wrongCredentials'))->withInput();
                }
            }
        } catch (Exception $e) {
            return $this->responce->ResponceExceptionError($e->message);
        }
    }

    public function getLogout() {
        Auth::logout();
        return redirect('admin/login');
    }

    public function normalUserList() {
        $userObj = new User();
        $userCountGroupByRelationshipStatus = $userObj->userCountGroupByRelationshipStatus($status = 0);
        $userCountGroupByGender = $userObj->userCountGroupByGender($status = 0);
        $userCountGroupByHeardAbout = $userObj->userCountGroupByHeardAbout($status = 0);
        $userTotalCountByStatus = $userObj->userCountGroupByStatus($status = 0);
        return view('normaluserlisting', compact('userCountGroupByRelationshipStatus', 'userTotalCountByStatus', 'userCountGroupByGender', 'userCountGroupByHeardAbout'));
    }

    public function accessedUserList() {
        $userObj = new User();
        $userCountGroupByRelationshipStatus = $userObj->userCountGroupByRelationshipStatus($status = 1);
        $userCountGroupByGender = $userObj->userCountGroupByGender($status = 1);
        $userTotalCountByStatus = $userObj->userCountGroupByStatus($status = 1);
        $userCountGroupByHeardAbout = $userObj->userCountGroupByHeardAbout($status = 1);
        return view('accessuserlisting', compact('userCountGroupByRelationshipStatus', 'userTotalCountByStatus', 'userCountGroupByGender', 'userCountGroupByHeardAbout'));
    }

    public function getAccessedUserList($flag = 0) {
        try {
            $objUser = new User();
            $users = $objUser->getUserListByFlag($flag);

            return Datatables::of($users)->make(true);
        } catch (Exception $e) {
            return $this->responce->ResponceExceptionError($e->message);
        }
    }

    public function getNormalUserList($flag = 0) {

        try {
            $objUser = new User();
            $users = $objUser->getUserListByFlag($flag);
            return Datatables::of($users)
                            ->addColumn('action', function ($user) {
                                return '<a href="grant-full-access/' . $user->user_id . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Grant Full Access</a>';
                            })
                            ->make(true);
        } catch (Exception $e) {
            return $this->responce->ResponceExceptionError($e->message);
        }
    }

    public function getAdminProfile() {
        return view('profile');
    }

    public function getAdminChangeEmail() {
        return view('changeemail');
    }

    public function getThoughtHistory() {
        return view('history');
    }

    public function postAdminChangeEmail(Request $request) {
        $email = $request->email;
        try {
            $validation_rules = array(
                'email' => 'email|required|unique:users'
            );
            $messages = array(
                'email.unique' => trans('messages.emailAlreadyExist'),
            );
            $validator = Validator::make(
                            $request->all()
                            , $validation_rules, $messages
            );

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            } else {

                $user = new User;
                $userUpdateEmail = $user->updateEmail($email);
                if ($userUpdateEmail) {
                    return Redirect('admin/change-email')->with('message', trans('messages.emailChanged'));
                } else {
                    return Redirect::back()->withErrors(trans('messages.wrongCredentials'))->withInput();
                }
            }
        } catch (Exception $e) {
            return $this->responce->ResponceExceptionError($e->message);
        }
    }

    public function postChangePassword(Request $request) {
        try {
            $validator = Validator::make($request->all(), array(
                        'currentPassword' => 'required',
                        'newPassword' => 'required|min:6',
                        'repeatPassword' => 'required|same:newPassword'
                            )
            );

            if ($validator->fails()) {
                return Redirect::back()
                                ->withErrors($validator);
            } else {
                $user = User::find(Auth::user()->user_id);
                $currentPassword = $request->currentPassword;
                $newPassword = $request->newPassword;
                if (Hash::check($currentPassword, $user->password)) {
                    $user->password = Hash::make($newPassword);

                    /*  save the new password */
                    if ($user->save()) {
                        return Redirect::back()
                                        ->with('message', trans('messages.passwordChanged'));
                    }
                } else {
                    return Redirect::back()->
                                    withErrors(trans('messages.passwordchangederror'));
                }
            }

            /* fall back */
            return Redirect::back()
                            ->with('message', trans('messages.passwordCouldNotBeChanged'));
        } catch (Exception $e) {
            return $this->responce->ResponceExceptionError($e->message);
        }
    }

    public function grantFullAccess($id = 0) {
        if (!empty($id)) {
            $user = new User;
            $grantfullaccess = $user->grantFullAccess($id);
            if ($grantfullaccess) {
                return Redirect::back()
                                ->with('message', trans('messages.fullAcceesGranted'));
            } else {
                return Redirect::back()
                                ->with('message', trans('messages.someError'));
            }
        }
    }

    public function getThoughtList() {
        return view('thoughtslisting');
    }

    public function postThoughtsList() {
        try {
            $categories = Categories::select('category_id', 'category_name')->where('status', 0);

            return Datatables::of($categories)
                            ->addColumn('action', function ($category) {
                                return '<a href="edit-thoughs/' . $category->category_id . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                            })
                            ->make(true);
        } catch (Exception $e) {
            return $this->responce->ResponceExceptionError($e->message);
        }
    }

    public function getThoughtHistoryList($fromdate = '', $todate = '',$gender='',$min_age='',$max_age='') {
        try {
            $q = UserTap::query();

            if (!empty($fromdate) && !empty($todate)) {

                $q->whereBetween('user_tap.date', [date('Y-m-d',strtotime($fromdate)),date('Y-m-d',strtotime($todate))]);
            }
            if(!empty($gender))
            {
             $q->where('users.gender',$gender);   
            }
            if(!empty($min_age) && empty($max_age))
            {
             $q->where('users.age','<=',intval($min_age));   
            }
            if(!empty($max_age) && empty($min_age))
            {
             $q->where('users.age','>=',intval($max_age));   
            }
            if(!empty($min_age) && !empty($max_age))
            {
             $q->whereBetween('users.age', [intval($min_age),intval($max_age)]);  
            }
            $q->where('user_tap.category_count','!=',0);
            $thought_count = $q->groupBy('user_tap.category_id')
                    ->leftjoin('categories', 'user_tap.category_id', '=', 'categories.category_id')
                    ->join('users','users.user_id','=','user_tap.userid')
                    ->leftjoin('user_custom_thought', 'user_custom_thought.id', '=', 'user_tap.category_id')
                    ->selectRaw('count(distinct user_tap.userid) as numberofusers')
                    ->selectRaw('sum(user_tap.category_count) as sum,ifnull(categories.category_name,user_custom_thought.thought_name) as thoughtname')
                    ->selectRaw('ROUND(sum(user_tap.category_count)/count(distinct user_tap.userid),2) as avaragethoughts')
                    ;
            return Datatables::of($thought_count)->make(true);
        } catch (Exception $e) {
            return $this->responce->ResponceExceptionError($e->message);
        }
    }

    public function editThoughs($thoughtId) {
        $objCategory = new Categories;
        $getThoughbyid = $objCategory->getCategoryById($thoughtId)->toArray();
        return view('editthought')->with('thoughtdata', $getThoughbyid);
    }

}
