<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */


Route::get('/', function () {
    return view('welcome');
});

Route::controllers([
    'password' => 'Auth\PasswordController',
]);
Route::group(['prefix' => 'api/v1'], function() {
    Route::post('registeration', 'api\v1\UserApiController@postRegisteration');
    Route::post('login', 'api\v1\UserApiController@postLogin');
    Route::post('logout', 'api\v1\UserApiController@postLogout');
        Route::post('insert-education-address', 'api\v1\UserSelectedCategoryController@postInsertEducationAddress');

});
