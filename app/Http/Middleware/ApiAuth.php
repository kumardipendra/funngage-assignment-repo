<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\UserToken;
class ApiAuth {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed 
     */
    public function handle($request, Closure $next) {
        $data = json_decode($request->getContent(), true);
        if(empty($data))
        {
          $data['userId'] =  $request['userId']; 
          $data['userAccessToken'] = $request['userAccessToken'];
        }
        
        $user = UserToken::where('user_id', $data['userId'])
                ->where('user_access_token', $data['userAccessToken'])
                ->first();

        if (!is_object($user)) {
            $response = array(
                'status' => "0",
                'response_code' => "604",
                'data' => array(),
                'message' => trans('messages.invalidUser'),
            );
            return json_encode($response);
            
        }

        return $next($request);
    }

}
