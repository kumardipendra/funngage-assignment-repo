<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use trans;
use App\Model\User;

class ThoughtTracker extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:thoughttracker';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send thought tracker notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $tokenlist = User::select(['user_token.device_token as device_token','user_token.device_type as device_type'])
            ->join('user_token','users.user_id','=','user_token.user_id')
            ->join('user_tap','users.user_id','=','user_tap.userid')
                ->where('users.notification','1')
                ->whereNotNull('user_token.device_token')
                ->where('users.notification','1')
                ->whereRaw('user_tap.date >= DATE(NOW() - INTERVAL 7 DAY)')->get();
        
        $messageios = PushNotification::Message(trans('messages.pushNotificationMessage'), array(
                    'badge' => 1,
                    'sound' => 'default'
        ));
        foreach ($tokenlist as $newtokenlist) {
            $deviceType = 'appNameAndroid';
            if (strtolower($newtokenlist->device_type) == 'ios') {
                $deviceType = 'appNameIOS';
            }
            PushNotification::app($deviceType)->to($newtokenlist->device_token)
                    ->send($messageios);
        }
        $this->info(trans('messages.thoughtTrackerNotification'));
    }
}
