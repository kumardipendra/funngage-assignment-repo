<?php

return
        [
            
            'sucessfullylogout' => 'You have successfully logged out.',
            'wrongEmail' => 'Wrong email.',
            'invalidUser' => 'The user is not registered with us',
            'notemptyeducation' => "Education should not be empty.",
            'notemptyaddress' => "Address should not be empty.",
            'validuserid' => "User id does not exists.",
            'insertedsucessfully'=>'Education and Address inserted sucessfully.'
            
];
