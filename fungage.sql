-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 26, 2017 at 11:03 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fungage`
--

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

CREATE TABLE `education` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `degree` varchar(50) NOT NULL,
  `university` varchar(50) NOT NULL,
  `college` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `education`
--

INSERT INTO `education` (`id`, `year`, `degree`, `university`, `college`) VALUES
(1, 2005, 'MCA', 'DU', 'ramjas'),
(2, 2009, 'MBA', 'DU', 'ramjas');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '''0''=>''Active'',''1''=>''Deactive''',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '''0;=>''No,''1''=>''Yes''',
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `email`, `password`, `status`, `is_deleted`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'dipendra.kumar@appster.in', '$2y$10$Dr2MSqUiDz5.akAJCs.Im.LLX/Q9.lWfXtGiKiotQplPKoXRzV2P2', 0, 0, NULL, '2017-05-22 19:16:10', '2017-05-22 19:17:26'),
(2, 'dipendra.kumar2@gmail.com', '$2y$10$Z63xD6I78X4u5RDDLzC8t.uNt4WnqSTJgc7mYlcV4oFPR/nBmc7xG', 0, 0, NULL, '2017-05-22 19:17:59', '2017-05-22 13:47:59'),
(3, 'dipendra.kumar22@gmail.com', '$2y$10$z5if3PVhlOgj5V195eZjcelFAeOFVUSvAbpaGPZvsAjb6JvhvmVZu', 0, 0, NULL, '2017-05-22 19:19:06', '2017-05-22 13:49:06'),
(4, 'dipendra.kumar222@gmail.com', '$2y$10$L8BMYs.qdwHu0neOVCL7yeXlJpXnC0muoty/nS0OHtvt/9Mvw4iz.', 0, 0, NULL, '2017-05-22 19:20:26', '2017-05-22 13:50:26'),
(5, 'dipendra.kumar2212@gmail.com', '$2y$10$64MggTju0we/TD8UUU7ogekMnueA5Ur/GTuD8lW49X7poM0wYMZHq', 0, 0, NULL, '2017-05-22 19:23:47', '2017-05-22 13:53:47'),
(6, 'dipendra.kumar22112@gmail.com', '$2y$10$lw.l/Mfdw5nc0wP05H7aXOBiNHfGHW5NcHH.PgxDn.mkodMNiZPXC', 0, 0, NULL, '2017-05-22 19:24:12', '2017-05-22 13:54:12');

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE `user_address` (
  `id` int(11) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `pincode` int(6) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_address`
--

INSERT INTO `user_address` (`id`, `address1`, `pincode`, `city`, `state`) VALUES
(1, 'c 88 ganganagar', 250001, 'meerut', 'UP');

-- --------------------------------------------------------

--
-- Table structure for table `user_token`
--

CREATE TABLE `user_token` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_token` varchar(255) DEFAULT NULL,
  `user_access_token` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_token`
--

INSERT INTO `user_token` (`id`, `user_id`, `device_token`, `user_access_token`, `created_at`, `updated_at`) VALUES
(0, 2, NULL, '755ab9b6f442703d4b945907d3dccce5', '2017-05-26 21:00:42', '2017-05-26 15:30:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `education`
--
ALTER TABLE `education`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `user_address`
--
ALTER TABLE `user_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `education`
--
ALTER TABLE `education`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user_address`
--
ALTER TABLE `user_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
